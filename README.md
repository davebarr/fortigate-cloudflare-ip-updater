# fortigate-cloudflare-ip-updater

### Requirements

Go 1.16

### Installation

```sh
git clone https://gitlab.com/davebarr/fortigate-cloudflare-ip-updater
cd fortigate-cloudflare-ip-updater
go build
```

### Usage

```
Usage:
  fortigate-cloudflare-ip-updater [OPTIONS]

Application Options:
  -u, --url=         Fortigate UI URL [$FORTIGATE_URL]
  -a, --accesstoken= Fortigate API Access Token [$FORTIGATE_ACCESS_TOKEN]
  -i, --insecure     Ignore TLS certificate verification errors [$FORTIGATE_INSECURE]
  -4, --v4name=      Cloudflare IPv4 Address Group Name (default: Cloudflare IPv4 IPs) [$CLOUDFLARE_IPV4_ADDRESS_GROUP_NAME]
  -6, --v6name=      Cloudflare IPv6 Address Group Name (default: Cloudflare IPv6 IPs) [$CLOUDFLARE_IPV6_ADDRESS_GROUP_NAME]
  -c, --color=       Address color number (default: 0) [$FORTIGATE_ADDRESS_COLOUR]

Help Options:
  -h, --help         Show this help message
```

### Example
```sh
export FORTIGATE_ACCESS_TOKEN=asdf
./fortigate-cloudflare-ip-updater --url https://192.0.2.1/ --insecure

2021/05/14 15:06:06 Found address group [Cloudflare IPv4 IPs]
2021/05/14 15:06:06 - 173.245.48.0/20
2021/05/14 15:06:06 - 103.21.244.0/22
2021/05/14 15:06:06 - 103.22.200.0/22
2021/05/14 15:06:06 - 103.31.4.0/22
2021/05/14 15:06:06 - 141.101.64.0/18
2021/05/14 15:06:06 - 108.162.192.0/18
2021/05/14 15:06:06 - 190.93.240.0/20
2021/05/14 15:06:06 - 188.114.96.0/20
2021/05/14 15:06:06 - 197.234.240.0/22
2021/05/14 15:06:06 - 198.41.128.0/17
2021/05/14 15:06:06 - 162.158.0.0/15
2021/05/14 15:06:06 - 104.16.0.0/13
2021/05/14 15:06:06 - 104.24.0.0/14
2021/05/14 15:06:06 - 172.64.0.0/13
2021/05/14 15:06:06 - 131.0.72.0/22
2021/05/14 15:06:06 IPv4 address group is in sync
2021/05/14 15:06:06 Found address group [Cloudflare IPv6 IPs]
2021/05/14 15:06:06 - 2a06:98c0::/29
2021/05/14 15:06:06 - 2400:cb00::/32
2021/05/14 15:06:06 - 2606:4700::/32
2021/05/14 15:06:06 - 2803:f800::/32
2021/05/14 15:06:06 - 2405:b500::/32
2021/05/14 15:06:06 - 2405:8100::/32
2021/05/14 15:06:06 - 2c0f:f248::/32
2021/05/14 15:06:06 IPv6 address group is in sync
```

