package main

import (
    "bytes"
    "crypto/tls"
    "encoding/json"
    "errors"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "net/http/cookiejar"
    "net/url"
    "os"
    "strings"

    "github.com/jessevdk/go-flags"
    "golang.org/x/net/publicsuffix"
)

var opts struct {
	Url string `short:"u" long:"url" description:"Fortigate UI URL" required:"true" env:"FORTIGATE_URL"`
	AccessToken string `short:"a" long:"accesstoken" description:"Fortigate API Access Token" required:"true" env:"FORTIGATE_ACCESS_TOKEN"`
	Insecure bool `short:"i" long:"insecure" description:"Ignore TLS certificate verification errors" env:"FORTIGATE_INSECURE"`
	CloudflareIpv4AddrGroupName string `short:"4" long:"v4name" description:"Cloudflare IPv4 Address Group Name" default:"Cloudflare IPv4 IPs" env:"CLOUDFLARE_IPV4_ADDRESS_GROUP_NAME"`
	CloudflareIpv6AddrGroupName string `short:"6" long:"v6name" description:"Cloudflare IPv6 Address Group Name" default:"Cloudflare IPv6 IPs" env:"CLOUDFLARE_IPV6_ADDRESS_GROUP_NAME"`
	Color string `short:"c" long:"color" description:"Address color number" default:"0" env:"FORTIGATE_ADDRESS_COLOUR"`
}

func httpGet(client *http.Client, url string) []byte {
    resp, err := client.Get(url)

    if err != nil {
        log.Fatal(err)
    }

    defer func() {
        _ = resp.Body.Close()
    }()
    body, err := ioutil.ReadAll(resp.Body)

    if err != nil {
        log.Fatal(err)
    }

    return body
}

type member struct {
    Name string `json:"name"`
}

type results struct {
    Name string
    Member []member
}

type addressResponse struct {
    Results []results
}

type cloudflareResult struct {
    V4 []string `json:"ipv4_cidrs"`
    V6 []string `json:"ipv6_cidrs"`
}

type cloudflareResponse struct {
    Result cloudflareResult
}

func main() {
    var err error
    var client *http.Client
    var cfResponse cloudflareResponse
    var fgV4Ips []string
    var fgV6Ips []string

    if _, err := flags.ParseArgs(&opts, os.Args); err != nil {
        return
    }

    if client, err = buildClient(); err != nil {
        log.Fatal(err)
    }

    if cfResponse, err = getCloudflareIps(client); err != nil {
        log.Fatal(err)
    }

    if fgV4Ips, err = getV4AddressGroup(client); err != nil {
        log.Fatal(err)
    }

    syncAddrGroup(client, "IPv4", cfResponse.Result.V4, fgV4Ips)

    if fgV6Ips, err = getV6AddressGroup(client); err != nil {
        log.Fatal(err)
    }

    syncAddrGroup(client, "IPv6", cfResponse.Result.V6, fgV6Ips)
}

func getV4AddressGroup(client *http.Client) ([]string, error) {
    var err error
    var response addressResponse

    params := make(url.Values)
    params.Add("access_token", opts.AccessToken)
    params.Add("filter", "name=="+opts.CloudflareIpv4AddrGroupName)

    addressv4GroupResponse := httpGet(client, opts.Url+"/api/v2/cmdb/firewall/addrgrp?"+params.Encode())

    if err = json.Unmarshal(addressv4GroupResponse, &response); err != nil {
        return nil, errors.New(fmt.Sprintf("Could not interpret v4 JSON: %v\n", err))
    }

    addrGroup := getAddressGroup(opts.CloudflareIpv4AddrGroupName, response)
    return addrGroup, nil
}

func getV6AddressGroup(client *http.Client) ([]string, error) {
    var err error
    var response addressResponse

    params := make(url.Values)
    params.Add("access_token", opts.AccessToken)
    params.Add("filter", "name=="+opts.CloudflareIpv6AddrGroupName)

    addressv6GroupResponse := httpGet(client, opts.Url+"/api/v2/cmdb/firewall/addrgrp6?"+params.Encode())

    if err = json.Unmarshal(addressv6GroupResponse, &response); err != nil {
        return nil, errors.New(fmt.Sprintf("Could not interpret v6 JSON: %v\n", err))
    }

    addrGroup := getAddressGroup(opts.CloudflareIpv6AddrGroupName, response)
    return addrGroup, nil
}

func getCloudflareIps(client *http.Client) (cloudflareResponse, error) {
    var err error

    cfHttpResponse := httpGet(client, "https://api.cloudflare.com/client/v4/ips")

    var cfResponse cloudflareResponse

    if err = json.Unmarshal(cfHttpResponse, &cfResponse); err != nil {
        return cfResponse, errors.New(fmt.Sprintf("Could not interpret Cloudflare JSON: %v\n", err))
    }

    if len(cfResponse.Result.V4) < 5 {
        return cfResponse, errors.New(fmt.Sprintf("Cloudflare V4 response only contained %d entries\n", len(cfResponse.Result.V4)))
    }

    if len(cfResponse.Result.V6) < 5 {
        return cfResponse, errors.New(fmt.Sprintf("Cloudflare V6 response only contained %d entries\n", len(cfResponse.Result.V6)))
    }

    return cfResponse, nil
}

func difference(a, b []string) []string {
    var diff []string

    mb := make(map[string]struct{}, len(b))

    for _, x := range b {
        mb[x] = struct{}{}
    }

    for _, x := range a {
        if _, found := mb[x]; !found {
            diff = append(diff, x)
        }
    }

    return diff
}

func syncAddrGroup(client *http.Client, ipType string, ips []string, addrGroup []string) {
    cfToFgDiffs := difference(ips, addrGroup)
    fgToCfDiffs := difference(addrGroup, ips)

    if len(cfToFgDiffs) == 0 && len(fgToCfDiffs) == 0 {
        log.Printf("%s address group is in sync\n", ipType)
        return
    }

    if len(cfToFgDiffs) > 0 {
        log.Printf("%s IPs found on Cloudflare but not Fortigate: %v\n", ipType, cfToFgDiffs)
        for _, ip := range cfToFgDiffs {
            log.Printf("Creating %s\n", ip)

            if err := createAddress(ipType, client, ip); err != nil {
                log.Printf("Error creating address: %s\n", err)
            }

            log.Printf("Adding %s to group\n", ip)
        }
    }

    if err := setAddressGroup(ipType, client, ips); err != nil {
        log.Printf("Error setting address group: %s\n", err)
    }

    if len(fgToCfDiffs) > 0 {
        log.Printf("%s IPs found on Fortigate but not Cloudflare: %v\n", ipType, fgToCfDiffs)

        for _, ip := range fgToCfDiffs {
            log.Printf("Removing address: %s\n", ip)
            if err := removeAddress(ipType, client, ip); err != nil {
                log.Printf("Error removing address: %s\n", err)
            }
        }
    }
}

func getAddressGroup(addressGroupName string, response addressResponse) []string {
    var ips []string
    if len(response.Results) != 1 {
        log.Printf("Could not find address group [%s]\n", addressGroupName)
        return nil
    }

    i := response.Results[0]

    log.Printf("Found address group [%s]\n", i.Name)

    for _, m := range i.Member {
        if len(m.Name) > 3 && m.Name[0:3] == "cf_" {
            m.Name = strings.Replace(m.Name[3:], "_", "/", -1)
        }

        log.Printf("- %s\n", m.Name)
        ips = append(ips, m.Name)
    }

    return ips
}

type createAddressV4Request struct {
    Color string `json:"color"`
    Comment string `json:"comment"`
    Name string `json:"name"`
    Subnet string `json:"subnet"`
}

type createAddressV6Request struct {
    Color string `json:"color"`
    Comment string `json:"comment"`
    Name string `json:"name"`
    Ip6 string `json:"ip6"`
}

func createAddress(ipType string, client *http.Client, ip string) error {
    var httpResponse *http.Response
    var err error
    var body []byte
    var addressUrl string

    ipName := strings.Replace(ip, "/", "_", -1)

    if ipType == "IPv4" {
        addressUrl = "/api/v2/cmdb/firewall/address?access_token="+opts.AccessToken
        req := &createAddressV4Request{
            Color:   opts.Color,
            Comment: "Managed by fortigate-cloudflare-ip-updater",
            Name:    "cf_" + ipName,
            Subnet:  ip,
        }
        body, _ = json.Marshal(req)
    } else {
        addressUrl = "/api/v2/cmdb/firewall/address6?access_token="+opts.AccessToken
        req := &createAddressV6Request{
            Color:   opts.Color,
            Comment: "Managed by fortigate-cloudflare-ip-updater",
            Name:    "cf_" + ipName,
            Ip6:     ip,
        }
        body, _ = json.Marshal(req)
    }
    u := bytes.NewReader(body)

    if httpResponse, err = client.Post(opts.Url+addressUrl, "application/json", u); err != nil {
        return err
    }

    defer func() {
        _ = httpResponse.Body.Close()
    }()

    if body, err = ioutil.ReadAll(httpResponse.Body); err != nil {
        return err
    }

    log.Printf("%v\n", string(body))
    return nil
}

type addAddressToGroupRequest struct {
    Member []member `json:"member"`
}

func setAddressGroup(ipType string, client *http.Client, ips []string) error {
    var httpResponse *http.Response
    var err error
    var body []byte
    var addressUrl string

    addressReq := &addAddressToGroupRequest{
        Member: []member{},
    }

    for _, ip := range ips {
        ip = strings.Replace(ip, "/", "_", -1)
        addressReq.Member = append(addressReq.Member, member{Name: "cf_"+ip})
    }

    if ipType == "IPv4" {
        addressUrl = "/api/v2/cmdb/firewall/addrgrp/"+opts.CloudflareIpv4AddrGroupName+"?access_token="+opts.AccessToken
    } else {
        addressUrl = "/api/v2/cmdb/firewall/addrgrp6/"+opts.CloudflareIpv6AddrGroupName+"?access_token="+opts.AccessToken
    }

    body, _ = json.Marshal(addressReq)
    log.Printf("body: %v\n", string(body))
    u := bytes.NewReader(body)

	req, err := http.NewRequest("PUT", opts.Url+addressUrl, u)

	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	httpResponse, err = client.Do(req)

    defer func() {
        _ = httpResponse.Body.Close()
    }()

    if body, err = ioutil.ReadAll(httpResponse.Body); err != nil {
        return err
    }

    log.Printf("%v\n", string(body))
    return nil
}

func removeAddress(ipType string, client *http.Client, ip string) error {
    var httpResponse *http.Response
    var err error
    var body []byte
    var addressUrl string

    ip = strings.Replace(ip, "/", "_", -1)

    if ipType == "IPv4" {
        addressUrl = "/api/v2/cmdb/firewall/address/cf_"+ip+"?access_token="+opts.AccessToken
    } else {
        addressUrl = "/api/v2/cmdb/firewall/address6/cf_"+ip+"?access_token="+opts.AccessToken
    }

    u := bytes.NewReader(body)
	req, err := http.NewRequest("DELETE", opts.Url+addressUrl, u)

	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	httpResponse, err = client.Do(req)

    defer func() {
        _ = httpResponse.Body.Close()
    }()

    if body, err = ioutil.ReadAll(httpResponse.Body); err != nil {
        return err
    }

    log.Printf("%s: %v\n", addressUrl, string(body))
    return nil
}

func buildClient() (*http.Client, error) {
    tr := &http.Transport{
        TLSClientConfig: &tls.Config{InsecureSkipVerify: opts.Insecure},
    }

    jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
    if err != nil {
        return nil, err
    }

    client := &http.Client{
        Transport: tr,
        Jar:       jar,
    }

    return client, nil
}
