module gitlab.com/davebarr/fortigate-cloudflare-ip-updater

go 1.16

require (
	github.com/jessevdk/go-flags v1.5.0
	golang.org/x/net v0.0.0-20210510120150-4163338589ed
)
